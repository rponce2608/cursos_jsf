package com.mitocode.service;

import java.util.List;

import com.acme.model.Curso;

public interface IService {
	List<Curso> listar();
}
