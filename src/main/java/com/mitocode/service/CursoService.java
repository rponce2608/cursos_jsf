package com.mitocode.service;

import java.io.Serializable;
import java.util.List;

import com.acme.model.Curso;
import com.mitocode.dao.IDAO;

public class CursoService implements IService, Serializable {

	private IDAO dao;

	public CursoService(IDAO idao) {
		dao = idao;
	}

	@Override
	public List<Curso> listar() {
		return dao.listar();
	}

}
