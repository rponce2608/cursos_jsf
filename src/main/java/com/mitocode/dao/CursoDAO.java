package com.mitocode.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.acme.model.Curso;

public class CursoDAO implements IDAO, Serializable {

	@Override
	public List<Curso> listar() {
		List<Curso> lista = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			Curso cur = new Curso();
			cur.setIdCurso(i);
			cur.setNombre("Matematica");
			cur.setHoras(4);
			lista.add(cur);
		}

		return lista;
	}
}
