package com.mitocode.dao;

import java.util.List;

import com.acme.model.Curso;

public interface IDAO {
	List<Curso> listar();
}
