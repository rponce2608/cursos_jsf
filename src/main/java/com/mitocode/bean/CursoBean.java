package com.mitocode.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.acme.model.Curso;
import com.mitocode.dao.CursoDAO;
import com.mitocode.service.CursoService;
import com.mitocode.service.IService;

@Named
@ViewScoped
public class CursoBean implements Serializable {
	private List<Curso> lista;
	private Curso curso;

	private IService service;

	public CursoBean() {
		lista = new ArrayList<>();
		service = new CursoService(new CursoDAO());
		this.listar();
	}

	public void listar() {
		this.lista = service.listar();
	}

	public void enviar(Curso cur) {
		this.curso = cur;
	}

	public List<Curso> getLista() {
		return lista;
	}

	public void setLista(List<Curso> lista) {
		this.lista = lista;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

}
